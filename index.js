console.log("hello")

let students = []
let sectionArray = []

function addStudent(student) {
	const capitalName = student.charAt(0).toUpperCase() + student.slice(1)
	students.push(capitalName)
	console.log(capitalName + " was added.")
}

function countStudents() {
	console.log("There are currently " + students.length + " right now.")
}

function printStudents() {
	const sortedStudents = [...students]
	sortedStudents.sort()

	console.log("The students are: ")
	sortedStudents.forEach((student) => {
		console.log(student)
	})
}

function findStudent(student) {
	const foundStudents = students.filter((s) => {
		return student.toLowerCase() === s.toLowerCase()
	})

	const capitalName = student.charAt(0).toUpperCase() + student.slice(1)

	if(foundStudents.length === 0) {
		console.log(capitalName + " is not an enrollee.")
	} else if(foundStudents.length > 1) {
		console.log(capitalName + " are enrollees.")
	} else if(foundStudents.length === 1) {
		console.log(capitalName + " is an enrollee.")
	}
}

function addSection(section) {
	sectionArray = students.map((student) => {
		return student + " - section " + section 
	})

	console.log(sectionArray)
}

function removeStudent(student) {
	const capitalName = student.charAt(0).toUpperCase() + student.slice(1)

	if(!students.includes(capitalName)){
		console.log("Student is not enrolled.")
		return
	}

	let studentIndex = students.indexOf(student)
	students.splice(studentIndex, 1)
	console.log(capitalName + " was removed.")
}